from django.conf import settings
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


def get_driver():
    # initialize options
    capabilities = DesiredCapabilities.CHROME
    options = webdriver.ChromeOptions()
    # pass in headless argument to options
    options.add_argument('--headless')
    # proxy
    options.add_argument('--proxy-server={0}:24000'.format(settings.PROXY_IP))
    capabilities = options.to_capabilities()
    # initialize driver
    url = 'http://{0}:4444/wd/hub'.format(settings.SELENIUM_HUB_IP)
    driver = webdriver.Remote(
            command_executor=url,
            desired_capabilities=capabilities
    )
    return driver


def get_amazon_product_data(key):
    driver = get_driver()
    try:
        print('Opening browser')
        driver.get("https://www.amazon.com/dp/%s" % key)

        add_to_cart_btn = WebDriverWait(driver, 10).until(
            EC.presence_of_element_located((By.ID, "add-to-cart-button"))
        )
        # name
        name_elm = driver.find_element(By.ID, "productTitle")
        name = name_elm.text
        # price
        price_elm = driver.find_element(By.ID, "priceblock_ourprice")
        price = price_elm.text
        # num_customer_reviews
        num_customer_reviews_elm = driver.find_element(
            By.ID, "acrCustomerReviewText")
        num_customer_reviews = num_customer_reviews_elm.text.split(' ')[0]
        # rate_customer_reviews
        rate_cus_reviews_elm = driver.find_element(By.ID, "acrPopover")
        rate_customer_reviews = rate_cus_reviews_elm.get_property('title')
        rate_customer_reviews = rate_customer_reviews.split(' ')[0]
        # num_customer_questions_answers
        try:
            answer_question = driver.find_element(By.ID, "askATFLink")
            num_cust_questions_answers = answer_question.text.split(' ')[0]
        except:
            num_cust_questions_answers = 0
        # in_stock
        print('Add to cart')
        add_to_cart_btn.click()
        cart_btn = WebDriverWait(driver, 5).until(
            EC.presence_of_element_located(
                (By.ID, "hlb-view-cart-announce")
            )
        )
        cart_btn.click()
        productAvalaible = driver.find_element_by_class_name(
            "sc-product-availability")
        in_stock = True if productAvalaible.text == 'In Stock' else False
        # result
        result = {
            'name': name,
            'price': price,
            'num_customer_reviews': num_customer_reviews,
            'rate_customer_reviews': rate_customer_reviews,
            'num_customer_questions_answers': num_cust_questions_answers,
            'in_stock': in_stock,
        }
    except Exception as ex:
        print(ex)
        result = {}
        pass
    finally:
        print('Process completed')
        driver.quit()
    return result
