from django.db import models
from django.db.models.signals import post_save
from django_mysql.models import JSONField

# Create your models here.


class Item(models.Model):
    key = models.CharField(max_length=32)
    name = models.CharField(max_length=255, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True, blank=True, editable=False)

    class Meta:
        verbose_name_plural = "Items"

    def __str__(self):
        return self.key


class ItemProperty(models.Model):
    item = models.OneToOneField(
        Item, related_name='item_properties', on_delete=models.CASCADE)
    data = JSONField(blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(null=True, blank=True, editable=False)

    class Meta:
        verbose_name_plural = "Item Properties"

    def __str__(self):
        return self.item.key


def create_item_property_callback(sender, instance, **kwargs):
    itemproperty, created = ItemProperty.objects.get_or_create(item=instance)
post_save.connect(create_item_property_callback, Item)
