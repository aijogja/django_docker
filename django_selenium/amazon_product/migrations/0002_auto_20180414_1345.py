# Generated by Django 2.0.4 on 2018-04-14 13:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('amazon_product', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
        migrations.AlterField(
            model_name='itemproperty',
            name='deleted',
            field=models.DateTimeField(blank=True, editable=False, null=True),
        ),
    ]
